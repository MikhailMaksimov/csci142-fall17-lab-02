package nearest;

import java.awt.Point;
import java.util.Vector;
import java.util.Collections;
import java.util.Comparator;

/**
 * The nearestPointsConcrete class is for measuring the smallest distance
 * between any two points in the vector. The test cases input the points
 * and this class finds the points with the shortest distance in between
 * them. This is meant to simulate an aviation program that tracks the 
 * locations of flying planes to make sure they do not get too close to
 * each other.
 * 
 * @author Mikhail Maksimov
 *
 */
public class NearestPointsConcrete implements NearestPoints
{
	private Vector<Point> myPoints;
	private double myShortestLength;
	private int mySmallestIndexLeft;
	private int mySmallestIndexRight;
	
	
	/**
	 * Constructor setting up the vector and a few variables
	 */
	public NearestPointsConcrete()
	{
		myPoints = new Vector<Point>();
		myShortestLength = Double.MAX_VALUE;
		mySmallestIndexLeft = NO_VALUE;
		mySmallestIndexRight = NO_VALUE;
	}
	
	
	/**
	 * This method should take the Vector<Point> of points added
	 * in this class and find the two points that are nearest.  
	 * The results should be accessed using the getters provided
	 * below.  This method MUST be recursive and O(nlg(n)) in 
	 * complexity using the method we discussed in class.
	 */
	public void solveNearest()
	{
		sort();
		recursiveSearch(0, myPoints.size()-1);	
	}
	
	
	/**
	 * Method for searching the vector for the shortest distance 
	 * between two points in a recursive fashion.
	 * 
	 * @param low
	 * @param high
	 */
	private void recursiveSearch(int low, int high)
    {	
        if (high>low)
        {
            int mid = low + (high - low)/2;
            
            Point p1 = myPoints.get(low);
            Point p2 = myPoints.get(high);
            double length = findDistance(p1, p2);
            
			if (length < myShortestLength)
            {
				myShortestLength = length;
				mySmallestIndexLeft = low;
				mySmallestIndexRight = high;
            }
            if (mid != low)
            {
            	recursiveSearch(mid, high);
            }
            
            if (mid != high)
            {
            	recursiveSearch(low, mid);
            } 
        }
    }
	
	
	/**
	 * This method should take the Vector<Point> of points added
	 * in this class and find the two points that are nearest.  
	 * The results should be accessed using the getters provided
	 * below.  This method is NOT recursive and O(n^2) in 
	 * complexity, as we discussed in class, by taking the first
	 * point and comparing the distance to every other point, then
	 * moving to the next point and so on.
	 */
	public void solveNearestExhaustive()
	{
		for (int index1 = 0; index1<myPoints.size()-1; index1++)
		{
			for (int index2 = index1+1; index2<myPoints.size(); index2++)
			{
				Point p1 = myPoints.get(index1);			
				Point p2 = myPoints.get(index2);
				double length = findDistance(p1, p2);
				
				if (length < myShortestLength)
				{
					myShortestLength = length;
					if (p1.getX() <= p2.getX())
					{
						mySmallestIndexLeft = index1;
						mySmallestIndexRight = index2;
					}
					if (p1.getX() > p2.getX())
					{
						mySmallestIndexLeft = index2;
						mySmallestIndexRight = index1;
					}
				}
			}
		}
	}
	
	
	/**
	 * Finds distance between 2 points
	 * 
	 * @param p1 First point
	 * @param p2 Second point
	 * @return The distance between the first and second point
	 */
	public double findDistance(Point p1, Point p2)
	{
		double x1 = p1.getX();
		double y1 = p1.getY();
		double x2 = p2.getX();
		double y2 = p2.getY();
		double length = Math.hypot(x1-x2, y1-y2);
		return length;
	}
	
	
	/**
	 * Method adds a point to the vector myPoints
	 * 
	 * @param point
	 */
	public void add(Point point)
	{
		myPoints.add(point);
	}
	
	
	/**
	 * Method gets the index of the left point of the two points with
	 * the shortest distance.
	 * 
	 * @return index of left point of shortest distance
	 */
	public int getSmallestIndexLeft()
	{
		return mySmallestIndexLeft;
	}
	
	
	/**
	 * Method gets the index of the right point of the two points with
	 * the shortest distance.
	 * 
	 * @return index of right point of shortest distance
	 */
	public int getSmallestIndexRight()
	{
		return mySmallestIndexRight;
	}

	
	/**
	 * Method for returning the shortest distance between any two points
	 * 
	 * @return the shortest distance
	 */
	public double getDistance()
	{	
		return myShortestLength;
	}
	
	
	/**
	 * Method for sorting the vector myPoints. It sorts the points
	 * in the vector in order of ascending x values.
	 */
	public void sort()
	{
		Collections.sort(myPoints,new Comparator<Point>()
		{
			public int compare(Point o1, Point o2)
			{
			    return Double.compare((o1.getX()), o2.getX());
			}
		});
	}
}
