package nearest;

/**
 * Interface for the NearestPointsConcrete class that tells
 * it what methods it needs to have.
 * 
 * @author Mikhail Maksimov
 *
 */
public interface NearestPoints
{
	/**
	 * This is a constant used as the initial value of some
	 * variables
	 */
	public final static int NO_VALUE = -1;
	
	
	/**
	 * This method should take the Vector<Point> of points added
	 * in this class and find the two points that are nearest.  
	 * The results should be accessed using the getters provided
	 * below.  This method MUST be recursive and O(nlg(n)) in 
	 * complexity using the method we discussed in class.
	 */
	public void solveNearest();
	
	
	/**
	 * This method should take the Vector<Point> of points added
	 * in this class and find the two points that are nearest.  
	 * The results should be accessed using the getters provided
	 * below.  This method is NOT recursive and O(n^2) in 
	 * complexity, as we discussed in class, by taking the first
	 * point and comparing the distance to every other point, then
	 * moving to the next point and so on.
	 */
	public void solveNearestExhaustive();
	
	/**
	 * Method for sorting the vector myPoints. It sorts the points
	 * in the vector in order of ascending x values.
	 */
	public void sort();
	
	
	/**
	 * Method gets the index of the left point of the two points with
	 * the shortest distance.
	 * 
	 * @return index of left point of shortest distance
	 */
	public int getSmallestIndexLeft();
	
	
	/**
	 * Method gets the index of the right point of the two points with
	 * the shortest distance.
	 * 
	 * @return index of right point of shortest distance
	 */
	public int getSmallestIndexRight();
	
	
	/**
	 * Method for returning the shortest distance between any two points
	 * 
	 * @return the shortest distance
	 */
	public double getDistance();
}
