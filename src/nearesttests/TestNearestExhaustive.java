package nearesttests;

import static org.junit.Assert.*;

import java.awt.Point;

import org.junit.Before;
import org.junit.Test;

import nearest.NearestPointsConcrete;

/**
 * Test cases for the exhaustive search algorithm that finds
 * the shortest distance between any two points. Points are NOT
 * sorted, so left and right indices might be different from the
 * binary search method.
 * 
 * @author Mikhail Maksimov
 *
 */
public class TestNearestExhaustive {
	private NearestPointsConcrete myNearestPointsConcrete;
	private double myError;
	
	/**
	 * myNearsetPointsConcrete will be initialized before running each test
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		myNearestPointsConcrete = new NearestPointsConcrete();
		myError = 0.00001;
	}
	
	/**
	 * Test is conducted with one point. The shortest distance cannot be 
	 * found with only one point, so the default values for all the variables
	 * are going to be retrieved.
	 */
	@Test
	public void testOnePoint() {
		myNearestPointsConcrete.add(new Point(5, 4));
		myNearestPointsConcrete.solveNearestExhaustive();
		assertEquals(NearestPointsConcrete.NO_VALUE, myNearestPointsConcrete.getSmallestIndexLeft());
		assertEquals(NearestPointsConcrete.NO_VALUE, myNearestPointsConcrete.getSmallestIndexRight());
		assertEquals(Double.MAX_VALUE, myNearestPointsConcrete.getDistance(), myError);
	}
	
	/**
	 * Two points with negative values are tested to make sure that the exhaustive
	 * algorithm can find the distance, even with negative numbers.
	 */
	@Test
	public void testTwoPointsNegs() {
		myNearestPointsConcrete.add(new Point(3, -4));
		myNearestPointsConcrete.add(new Point(-2, 2));
		myNearestPointsConcrete.solveNearestExhaustive();
		assertEquals(1, myNearestPointsConcrete.getSmallestIndexLeft());
		assertEquals(0, myNearestPointsConcrete.getSmallestIndexRight());
		assertEquals(7.81025, myNearestPointsConcrete.getDistance(), myError);
	}
	
	/**
	 * Test that the exhaustive algorithm can figure out the shortest distance
	 * with just 3 points.
	 */
	@Test
	public void testThreePoints() {
		myNearestPointsConcrete.add(new Point(5, 4));
		myNearestPointsConcrete.add(new Point(4, 4));
		myNearestPointsConcrete.add(new Point(3, 5));
		myNearestPointsConcrete.solveNearestExhaustive();
		assertEquals(1.0, myNearestPointsConcrete.getDistance(), myError);
	}
	
	/**
	 * Test that two non-adjacent points could have the shortest distance.
	 */
	@Test
	public void testSkippingOnePoint()
	{
		myNearestPointsConcrete.add(new Point(100,200));
		myNearestPointsConcrete.add(new Point(150,100));
		myNearestPointsConcrete.add(new Point(160,200));
		myNearestPointsConcrete.solveNearestExhaustive();
		assertEquals(60, myNearestPointsConcrete.getDistance(), myError);
		assertEquals(0, myNearestPointsConcrete.getSmallestIndexLeft());
		assertEquals(2, myNearestPointsConcrete.getSmallestIndexRight());
	}
	
	/**
	 * Tests a cluster of points to the left with two points on the 
	 * right that are very close to each other.
	 */
	@Test
	public void testClusterOfPointsWithTwoOnTheSide()
	{
		myNearestPointsConcrete.add(new Point(10,5));
		myNearestPointsConcrete.add(new Point(30,20));
		myNearestPointsConcrete.add(new Point(41,40));
		myNearestPointsConcrete.add(new Point(46,36));
		myNearestPointsConcrete.add(new Point(15,35));
		myNearestPointsConcrete.add(new Point(12,50));
		myNearestPointsConcrete.add(new Point(15,15));
		myNearestPointsConcrete.add(new Point(30,40));
		myNearestPointsConcrete.add(new Point(40,30));
		myNearestPointsConcrete.add(new Point(26,28));
		myNearestPointsConcrete.add(new Point(33,50));
		myNearestPointsConcrete.add(new Point(18,60));
		myNearestPointsConcrete.add(new Point(70,35));
		myNearestPointsConcrete.add(new Point(71,35));
		myNearestPointsConcrete.solveNearestExhaustive();
		assertEquals(1, myNearestPointsConcrete.getDistance(), myError);
		assertEquals(12, myNearestPointsConcrete.getSmallestIndexLeft());
		assertEquals(13, myNearestPointsConcrete.getSmallestIndexRight());
	}
	
	/**
	 * Test to find the shortest distance with a complicated assortment 
	 * of points that resembles a pyramid.
	 */
	@Test
	public void testPyramidShape()
	{
		myNearestPointsConcrete.add(new Point(50,35));
		myNearestPointsConcrete.add(new Point(55,25));
		myNearestPointsConcrete.add(new Point(65,35));
		myNearestPointsConcrete.add(new Point(50,50));
		myNearestPointsConcrete.add(new Point(52,20));
		myNearestPointsConcrete.add(new Point(35,35));
		myNearestPointsConcrete.add(new Point(20,50));
		myNearestPointsConcrete.add(new Point(45,25));
		myNearestPointsConcrete.add(new Point(80,50));
		myNearestPointsConcrete.solveNearestExhaustive();
		assertEquals(5.83095, myNearestPointsConcrete.getDistance(), myError);
		assertEquals(4, myNearestPointsConcrete.getSmallestIndexLeft());
		assertEquals(1, myNearestPointsConcrete.getSmallestIndexRight());
	}
	
	/**
	 * Test that the exhaustive algorithm can find 2 points
	 * that have the same x value but are on slightly different y values.
	 */
	@Test
	public void testOnePointAboveAnotherPoint()
	{
		myNearestPointsConcrete.add(new Point(60,60));
		myNearestPointsConcrete.add(new Point(40,60));
		myNearestPointsConcrete.add(new Point(75,40));
		myNearestPointsConcrete.add(new Point(80,55));
		myNearestPointsConcrete.add(new Point(70,65));
		myNearestPointsConcrete.add(new Point(50,55));
		myNearestPointsConcrete.add(new Point(65,50));
		myNearestPointsConcrete.add(new Point(60,65));
		myNearestPointsConcrete.solveNearestExhaustive();
		assertEquals(5, myNearestPointsConcrete.getDistance(), myError);
		assertEquals(0, myNearestPointsConcrete.getSmallestIndexLeft());
		assertEquals(7, myNearestPointsConcrete.getSmallestIndexRight());
	}
	
	/**
	 * Test that there are two pairs of points that have the same
	 * shortest distance. The exhaustive algorithm should pick
	 * the pair that it finds first.
	 */
	@Test
	public void testTwoPairsHaveTheSameDistance()
	{
		myNearestPointsConcrete.add(new Point(140,100));
		myNearestPointsConcrete.add(new Point(150,80));
		myNearestPointsConcrete.add(new Point(180,140));
		myNearestPointsConcrete.add(new Point(190,120));
		myNearestPointsConcrete.add(new Point(130,130));
		myNearestPointsConcrete.add(new Point(170,100));
		myNearestPointsConcrete.add(new Point(190,80));
		myNearestPointsConcrete.solveNearestExhaustive();
		assertEquals(22.36068, myNearestPointsConcrete.getDistance(), myError);
		assertEquals(0, myNearestPointsConcrete.getSmallestIndexLeft()); //also 2
		assertEquals(1, myNearestPointsConcrete.getSmallestIndexRight()); //also 3
	}
}
